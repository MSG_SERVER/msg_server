package com.thales.msg.util;

import java.util.Map;
import java.util.Map.Entry;
import java.util.function.Function;

public class Util {
//	public static void checkCommandLineArgs(String[] args) {
//		System.out.print("[");
//		for (String arg : args) {
//			System.out.print(arg + ";");
//		}
//		System.out.println("]");
//		char nextParam = ' ';
//		for (String arg : args) {
//			if (arg.startsWith("-")) {
//				arg = arg.substring(1);
//				if (arg.startsWith("p")) {
//					if (arg.startsWith("port")) {
//						arg = arg.substring(4);
//					} else {
//						arg = arg.substring(1);
//					}
//					arg = arg.trim();
//					if (arg.length() >= 1) {
//						setPort(Integer.parseInt(arg));
//					} else {
//						nextParam = 'p';
//					}
//				} else if (arg.startsWith("a")) {
//					if (arg.startsWith("adress")) {
//						arg = arg.substring(6);
//					} else {
//						arg = arg.substring(1);
//					}
//					arg = arg.trim();
//					if (arg.length() >= 7) {
//						setAdress(arg);
//					} else {
//						nextParam = 'a';
//					}
//				}
//			} else if (nextParam == 'p') {
//				nextParam = ' ';
//				arg = arg.trim();
//				if (arg.length() >= 1) {
//					setPort(Integer.parseInt(arg));
//				}
//			} else if (nextParam == 'a') {
//				nextParam = ' ';
//				arg = arg.trim();
//				if (arg.length() >= 7) {
//					setAdress(arg);
//				}
//			}
//		}
//	}
//
	public static void checkCommandLineArgs(String[] args, Map<String, Function<String, Boolean>> params) {
		String nextParam = "";
		for (String arg : args) {
			if (arg.startsWith("-")) {
				nextParam = "";
				arg = arg.substring(1).trim();
				for (Entry<String, Function<String, Boolean>> entry : params.entrySet()) {
					if (arg.matches(entry.getKey())) {
						boolean argumentValid = entry.getValue().apply(arg);
						if (!argumentValid) {
							nextParam = entry.getKey();
						}
					} else if (arg.matches("(" + entry.getKey() + ").+")) {

						boolean argumentValid = false;
						for (String regex : entry.getKey().split("\\|")) {
							regex = regex.trim();
							String argSplit[] = arg.split(regex);
							try {
								String arg2 = argSplit[argSplit.length - 1];
								argumentValid = entry.getValue().apply(arg2);
								if (argumentValid) {
									break;
								}
							} catch (ArrayIndexOutOfBoundsException e) {
								argumentValid = false;
							}
						}
						if (!argumentValid) {
							System.err.println("The Argument -" + arg + " is not valid");
						}
					}
				}
			} else {
				for (Entry<String, Function<String, Boolean>> entry : params.entrySet()) {
					if (nextParam.equals(entry.getKey())) {
						boolean argumentValid = entry.getValue().apply(arg);
						if (!argumentValid) {
							System.err.println("The Argument -" + arg + " is not valid");
						}
					}
				}
			}
		}
	}
}
