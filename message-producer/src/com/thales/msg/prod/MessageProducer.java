/**
 * 
 */
package com.thales.msg.prod;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.function.Function;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import com.thales.msg.util.Util;

/**
 * Message producer to send typed in messages to a message server.
 * 
 * The producer has to connect to the server using the message servers IP
 * address and the port number of the server intended for producers, before
 * messages can be sent.
 * 
 * The server address and port number are specified as a properties.
 * 
 * @author T0147441
 *
 */
public class MessageProducer {
	private static int port = 5454;
	private static String adress = "Martin";
	private static SocketChannel server;

	/**
	 * Start the message producer.
	 * 
	 * Read the server address and port number and open a client socket. (hint,
	 * maybe just use javax.net.SocketFacktory)
	 * 
	 * In an endless loop, read a text line from System.in and send it to the
	 * server.
	 * 
	 * 
	 */
	public static void main(String[] args) {
		checkCommandLineArgs(args);
		System.out.println("Working with: " + adress + ":" + port);

		boolean shouldReconnect = true;
		String connectionErrorMessage;
		Scanner scanner = new Scanner(System.in);
		while (shouldReconnect) {
			
			connectionErrorMessage = "Connecting to server was not possible. Would you like to try again?";

			try {
				server = SocketChannel.open(new InetSocketAddress(adress, port));
				System.out.println("Connection to Server at " + server.getLocalAddress() + " sucessfull!");
				while (true) {

					System.out.println("Please enter the Message you want to share with the consumers");
					String message = scanner.nextLine();
					try {
						sendMessage(message);
					} catch (IOException e) {
						connectionErrorMessage = "Connection to Server lost, would you like to reconnect";
						System.err.println("Could not deliver Message to Server at " + server.getLocalAddress());
						throw e;
					}
					System.out.println("\n-----------------------------------------------------------------");
				}
			} catch (IOException e1) {
				int dialogInput = showConfirmPopup(connectionErrorMessage, "Producer: Error trying to connect to server");
				switch (dialogInput) {
				// User wants to reconnect (pressed Yes)
				case 0:
					break;
				// User does not want to reconnect (pressed No or Cancel)
				default:
					shouldReconnect = false;
					break;

				}
			}
		
		}
		scanner.close();
		System.exit(0);

	}

	

	private static void checkCommandLineArgs(String[] args) {
		Map<String, Function<String, Boolean>> params = new HashMap<String, Function<String, Boolean>>();
		params.put("p|port", (s) -> setPort(s));
		params.put("a|adress", (s) -> setAdress(s));
		Util.checkCommandLineArgs(args, params);
	}



	public static boolean setPort(String port) {
		try {
			MessageProducer.port = Integer.parseInt(port);
			return true;
		}catch(NumberFormatException e) {
			return false;
		}
	}

	public static boolean setAdress(String adress) {
		if (isAdressValid(adress)) {
			MessageProducer.adress = adress;
			return true;
		}
		return false;
	}
	private static boolean isAdressValid(String adress) {
		return adress.matches("(([0-1][0-9]{0,2})|(25[0-5])|(2[0-4][0-9]))\\."
				+ "(([0-1][0-9]{0,2})|(25[0-5])|(2[0-4][0-9]))\\."
				+ "(([0-1][0-9]{0,2})|(25[0-5])|(2[0-4][0-9]))\\."
				+ "(([0-1][0-9]{0,2})|(25[0-5])|(2[0-4][0-9]))");
	}

	private static void sendMessage(String message) throws IOException {
		server.write(str_to_bb(message, Charset.forName("UTF-8")));
		System.out.println("Message sent: " + message);
	}

	public static ByteBuffer str_to_bb(String msg, Charset charset) {
		return ByteBuffer.wrap(msg.getBytes(charset));
	}

	private static int showConfirmPopup(String message, String title) {
		JFrame jframe = new JFrame();
		jframe.setAlwaysOnTop(true);
		jframe.setUndecorated(true);
		jframe.setLocationRelativeTo(null);
		int selection = JOptionPane.showConfirmDialog(jframe, message, title, JOptionPane.INFORMATION_MESSAGE);
		return selection;
	}

}
