# Aufgaben zum praktischen Ubungsteil JAVA

The idea is to implement 3 different programs:

- a message producer that reads lines from stdin and sends them out on a socket connected to a server
- a message server that receives messages from all producer clients and distributes them to consumer clients
- a message consumer that receives messages on a socket connected to a server and prints them on stdout.

Two of the 3 programs have been prepared and after cloning this repository they can be imported into eclipse.
There are README files with further instructions.

Preparing the 3rd program is left as an exercise.
