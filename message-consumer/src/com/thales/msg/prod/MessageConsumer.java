/**
 * 
 */
package com.thales.msg.prod;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import com.thales.msg.util.Util;

/**
 * Message producer to send typed in messages to a message server.
 * 
 * The producer has to connect to the server using the message servers IP
 * address and the port number of the server intended for producers, before
 * messages can be sent.
 * 
 * The server address and port number are specified as a properties.
 * 
 * @author T0147441
 *
 */
public class MessageConsumer {
	private static int port = 5455;
	private static String adress = "Jonathan";

	/**
	 * Start the message producer.
	 * 
	 * Read the server address and port number and open a client socket. (hint,
	 * maybe just use javax.net.SocketFacktory)
	 * 
	 * In an endless loop, read a text line from System.in and send it to the
	 * server.
	 * 
	 * @param args ignored
	 */
	public static void main(String[] args) {
		checkCommandLineArgs(args);

		System.out.println("Working with: " + adress + ":" + port);
		boolean shouldReconnect = true;
		String connectionErrorMessage;
		
		while (shouldReconnect) {
			connectionErrorMessage = "Connecting to server was not possible. Would you like to try again?";
			SocketChannel server;
			try {
				server = SocketChannel.open(new InetSocketAddress(adress, port));
				System.out.println("Connection to Server at " + server.getLocalAddress() + " sucessfull!");
				
				ByteBuffer buffer = ByteBuffer.allocate(1024);
				while (true) {
					buffer.clear();
					try {
						server.read(buffer);
						String message = bb_to_str(buffer, Charset.forName("UTF-8"));
						System.out.println("The message sent from the socket was: " + message);
						showPopupMessage(message);
					} catch (IOException e) {
						connectionErrorMessage = "Connection to Server lost, would you like to reconnect";
						System.out.println("Connection to Server at " + server.getLocalAddress() + " lost!");
						throw e;
					}
				}
			} catch (IOException e) {
				int dialogInput = showConfirmPopup(connectionErrorMessage, "Consumer: Error trying to connect to server");
				switch (dialogInput) {
				// User wants to reconnect (pressed Yes)
				case 0:
					break;
				// User does not want to reconnect (pressed No or Cancel)
				default:
					shouldReconnect = false;
					break;

				}
			}
		}
		System.exit(0);

	}

	private static void checkCommandLineArgs(String[] args) {
		Map<String, Function<String, Boolean>> params = new HashMap<String, Function<String, Boolean>>();
		params.put("p|port", (s) -> setPort(s));
		params.put("a|adress", (s) -> setAdress(s));
		Util.checkCommandLineArgs(args, params);
	}



	public static boolean setPort(String port) {
		try {
			MessageConsumer.port = Integer.parseInt(port);
			return true;
		}catch(NumberFormatException e) {
			return false;
		}
	}

	public static boolean setAdress(String adress) {
		InetSocketAddress inetAdress = new InetSocketAddress(adress, port);
		if (!inetAdress.isUnresolved()) {
			MessageConsumer.adress = adress;
			return true;
		}
		return false;
	}

	private static void showPopupMessage(String message) {
		JFrame jframe = new JFrame();
		jframe.setAlwaysOnTop(true);
		jframe.setUndecorated(true);
		jframe.setLocationRelativeTo(null);
		JOptionPane.showMessageDialog(jframe, message,
				"Message from message-server[address: " + adress + " port: " + port + "]", JOptionPane.PLAIN_MESSAGE);
	}

	private static int showConfirmPopup(String message, String title) {
		JFrame jframe = new JFrame();
		jframe.setAlwaysOnTop(true);
		jframe.setUndecorated(true);
		jframe.setLocationRelativeTo(null);
		int selection = JOptionPane.showConfirmDialog(jframe, message, title, JOptionPane.INFORMATION_MESSAGE);
		return selection;
	}

	public static String bb_to_str(ByteBuffer buffer, Charset charset) {
		byte[] bytes;
		if (buffer.hasArray()) {
			bytes = buffer.array();
		} else {
			bytes = new byte[buffer.remaining()];
			buffer.get(bytes);
		}
		return new String(bytes, charset);
	}

}
