/**
 * 
 */
package com.thales.msg.srv;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;

import com.thales.msg.util.Util;

/**
 * Message server to receive messages from producers and distribute them to
 * consumers.
 * 
 * The server opens
 * <ul>
 * <li>one listening TCP port for accepting client connections from message
 * producers
 * <li>one listening TCP port for accepting client connections from message
 * consumers
 * </ul>
 * 
 * The port numbers may be set as properties. Otherwise port numbers are chosen
 * by the underlying runtime. In any case the port numbers are reported on start
 * up.
 * 
 * @author T0147441
 *
 */
public class MessageServer {
	private static int port = 5454;
	private static int consumerPort = 5455;
	private static String message = "";

	public static void main(String[] args) {
		checkCommandLineArgs(args);
		try {
			Selector selector = Selector.open();
			//Socket for connections to consumers
			ServerSocketChannel consumerSocket = ServerSocketChannel.open();
			consumerSocket.bind(new InetSocketAddress(consumerPort));
			consumerSocket.configureBlocking(false);
			consumerSocket.register(selector, SelectionKey.OP_ACCEPT);
			//Socket for connections to producers
			ServerSocketChannel producerSocket = ServerSocketChannel.open();
			producerSocket.bind(new InetSocketAddress(port));
			producerSocket.configureBlocking(false);
			producerSocket.register(selector, SelectionKey.OP_ACCEPT);
			
			//ByteBuffer to buffer received messages
			ByteBuffer buffer = ByteBuffer.allocate(1024);
			System.out.println("Message Server avaiting for connections...");
			
			while (true) {
				selector.select();
				Set<SelectionKey> selectedKeys = selector.selectedKeys();
				Iterator<SelectionKey> iter = selectedKeys.iterator();
				while (iter.hasNext()) {
					SelectionKey key = iter.next();
					if (key.isAcceptable()) {
						try {
							if (key.channel() == producerSocket) {
								registerProducer(selector, producerSocket);
							} else if (key.channel() == consumerSocket) {
								registerConsumer(selector, consumerSocket, key);
							}
						} catch (IOException e) {
							e.printStackTrace();
						}
					}

					if (key.isValid() && key.isReadable()) {
						SocketChannel client = (SocketChannel) key.channel();
						buffer = ByteBuffer.allocate(1024);
						buffer.clear();
						try {
							if(client.read(buffer) == -1) {
								throw new IOException("Connection lost");	
							}
							message = bb_to_str(buffer, Charset.forName("UTF-8"));
							setNewMessageFlag(selector.keys(), producerSocket);
							System.out.println("Message received: " + message);
						} catch (IOException e) {
							//remove key to a lost connection
							key.cancel();
							System.out.println("Client " + client + " disconnected");
						}

					}
					if (key.isValid() && key.isWritable()) {
						//Check for the MessageFlag set when receiving a message
						if (key.attachment() != null && ((Boolean) key.attachment()).booleanValue()) {
							SocketChannel client = (SocketChannel) key.channel();
							try {
								client.write(str_to_bb(message, Charset.forName("UTF-8")));
								//setting Flag, so a client can not get the same message multiple times
								key.attach(new Boolean(false));
							} catch (IOException e) {
								//remove key to a lost connection
								key.cancel();
								System.out.println("Client " + client.getLocalAddress() + " disconnected");
								System.out.println("Could not deliver message to client " + client.getLocalAddress());
							}

						}
					}
					iter.remove();
				}

			}
		} catch (IOException e1) {
			e1.printStackTrace();
		}

	}

	public static ByteBuffer str_to_bb(String msg, Charset charset) {
		return ByteBuffer.wrap(msg.getBytes(charset));
	}

	public static String bb_to_str(ByteBuffer buffer, Charset charset) {
		byte[] bytes;
		if (buffer.hasArray()) {
			bytes = buffer.array();
		} else {
			bytes = new byte[buffer.remaining()];
			buffer.get(bytes);
		}
		return new String(bytes, charset);
	}
	
	private static void registerProducer(Selector selector, ServerSocketChannel serverSocket) throws IOException {
		SocketChannel client = serverSocket.accept();
		client.configureBlocking(false);
		client.register(selector, SelectionKey.OP_READ);
		System.out.println("Connection to " + client.getLocalAddress() + " sucessfull");
	}

	private static void registerConsumer(Selector selector, ServerSocketChannel serverSocket, SelectionKey key)
			throws IOException {
		SocketChannel client = serverSocket.accept();
		key.attach(new Boolean(false));
		client.configureBlocking(false);
		client.register(selector, SelectionKey.OP_WRITE);
		System.out.println("Connection to " + client.getLocalAddress() + " sucessfull");
	}
	
	private static void setNewMessageFlag(Set<SelectionKey> keys, ServerSocketChannel producerSocket) {
		for (SelectionKey key : keys) {
			if (key.channel() == producerSocket) {
				continue;
			}
			key.attach(new Boolean(true));
		}
	}

	private static void checkCommandLineArgs(String[] args) {
		Map<String, Function<String, Boolean>> params = new HashMap<String, Function<String, Boolean>>();
		params.put("p|port", (s) -> setPort(s));
		params.put("cp|consumerport|cport", (s) -> setConsumerPort(s));
		Util.checkCommandLineArgs(args, params);
	}
	public static boolean setPort(String port) {
		try {
			MessageServer.port = Integer.parseInt(port);
			return true;
		}catch(NumberFormatException e) {
			return false;
		}
	}

	private static boolean setConsumerPort(String consumerPort) {
		try {
			MessageServer.consumerPort = Integer.parseInt(consumerPort);
			return true;
		}catch(NumberFormatException e) {
			return false;
		}
	}

}
